use std::ops::{Add, Sub};

/// Point2D is a simple 2D point structure
///
/// ```rust
/// use circle::Point2D;
///
/// let p1 = Point2D::new(1.0, 0.0);
/// let p2 = Point2D::new(0.0, 1.0);
/// assert_eq!(format!("{:?}", p1 + p2), "Point2D { x: 1.0, y: 1.0 }");
/// ```
#[derive(Clone, Copy, Debug)]
pub struct Point2D {
    pub x: f32,
    pub y: f32,
}

impl Add for Point2D {
    type Output = Point2D;
    fn add(self, other: Point2D) -> Point2D {
        Point2D {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Point2D {
    type Output = Point2D;
    fn sub(self, other: Point2D) -> Point2D {
        Point2D {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Point2D {
    pub fn new(x: f32, y: f32) -> Point2D {
        Point2D { x, y }
    }
    pub fn origin() -> Point2D {
        Point2D { x: 0., y: 0. }
    }
    pub fn radial(radius: f32, angle: f32) -> Point2D {
        Point2D {
            x: radius * angle.cos(),
            y: radius * angle.sin(),
        }
    }
    pub fn abs(self) -> f32 {
        //! There is no trait for `abs`, one can expect a method with this name
        (self.x * self.x + self.y * self.y).sqrt()
    }
}
