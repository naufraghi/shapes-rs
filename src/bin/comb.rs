#![feature(proc_macro_hygiene)]

fn main() {
    //demo();
    let data = vec![1, 2, 3, 4, 5];
    let combinations2 = comb::combinations!(&data, 3);
    println!("comb::combinations!(&data, 3) = {:?}", combinations2);
}
