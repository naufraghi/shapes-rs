#![feature(slice_patterns)]

use rand::{seq::SliceRandom, thread_rng};
use std::{f32, iter};

use circle::*;

fn demo() -> () {
    fn circles(sizes: &[(f32, usize)]) -> Vec<Circle> {
        let radii = {
            let mut radii = vec![];
            for chunk in sizes.iter().map(|&(rad, num)| iter::repeat(rad).take(num)) {
                for rad in chunk {
                    radii.push(rad);
                }
            }
            radii.shuffle(&mut thread_rng());
            radii
        };
        let num = radii.len() as f32;
        let mut res = vec![];
        for (i, &radius) in radii.iter().enumerate() {
            let f = i as f32;
            let p = Point2D::radial(num, 2. * f32::consts::PI * f / num);
            res.push(Circle {
                center: p,
                radius: radius,
            });
        }
        res
    }
    let shapes = circles(&[(1., 20), (2., 10), (3., 4), (5., 2)]);
    let p = Placement::new(shapes);
    let mut e = Evolution::<Circle>::create(&p, 100);
    fn evolve(e: Evolution<Circle>, r: f32) -> Evolution<Circle> {
        (0..200).fold(e, |e, _| e.evolve(r))
    }
    let mut placements: Vec<String> = vec![];
    fn info(e: &Evolution<Circle>, placements: &mut Vec<String>) {
        println!("fitness {}", e.best().fitness());
        println!("valid {}", e.best().valid());
        let p = e.best();
        if let [s1, s2, s3] = p.shapes[..].windows(3).next().unwrap() {
            println!("overlap 1-2 {}", s1.overlap(s2));
            println!("overlap 2-3 {}", s2.overlap(s3));
            println!("overlap 1-3 {}", s1.overlap(s3));
        }
        let circles = p
            .shapes
            .iter()
            .map(circle_template)
            .collect::<Vec<_>>()
            .as_slice()
            .join(",\n ");
        placements.push(format!("[{}]", circles));
        elm_template(&placements.as_slice().join("\n    ,\n    "), 100, 100);
    }
    for i in 0..100 {
        e = evolve(e, 5.0 / (i as f32 + 1.));
        info(&e, &mut placements);
    }
}

fn elm_template(placements: &str, width: i32, height: i32) {
    use std::fs::File;
    use std::io::prelude::*;
    use std::io::BufWriter;
    let f = File::create("Circles.elm").expect("Unable to create Circles.elm");
    write!(
        BufWriter::new(f),
        r#"
module Main exposing (main)

import GraphicSVG exposing (..)

type Msg
    = Tick Float GetKeyState

type alias Circle =
    {{ radius : Float, x : Float, y : Float }}

type alias Placement =
    List Circle

type alias Model =
    {{ placements : List Placement, pos : Float, direction : Int }}


placements =
    [
    {placements}
    ]


init =
    {{ placements = placements, pos = 0, direction = 1 }}

update msg model =
    let
      cap pos =
        if pos < 0 then
           0
        else
           if pos >= List.length model.placements then
                (List.length model.placements) - 1
           else
                pos
    in
    case msg of
        Tick _ ( keys, _, _ ) ->
            case keys (Key "r") of
                JustDown ->
                    {{ model
                      | pos = cap (model.pos - model.direction)
                      , direction = -model.direction }}

                _ ->
                    {{ model | pos = cap (model.pos + model.direction) }}

blue_circle c =
    circle c.radius |> filled blue |> move (c.x, c.y)

view model =
    let
        placement = List.drop model.pos model.placements |> List.head
    in
    collage {width} {height}
        (case placement of
           Just p ->
              List.map blue_circle p
           _ ->
              []
        )

main =
    gameApp Tick {{
                   model = init
                 , update = update
                 , view = view
                 }}
"#,
        placements = placements,
        width = width,
        height = height
    )
    .expect("Unable to write Circles.elm")
}

fn circle_template(circle: &Circle) -> String {
    format!(
        "{{ radius = {radius}, x = {x}, y = {y} }}",
        radius = circle.radius,
        x = circle.center.x,
        y = circle.center.y
    )
}

fn main() {
    demo();
}
