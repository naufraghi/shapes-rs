#![recursion_limit = "128"]

extern crate proc_macro;
extern crate quote;
extern crate syn;

use proc_macro::TokenStream;
use quote::quote;
use syn::parse::{Parse, ParseStream, Result};
use syn::parse_macro_input;

struct Combinations {
    arg: syn::Expr,
    n: syn::LitInt,
}

impl Parse for Combinations {
    fn parse(input: ParseStream) -> Result<Self> {
        let arg = input.parse()?;
        input.parse::<syn::Token![,]>()?;
        let n = input.parse()?;
        Ok(Combinations { arg, n })
    }
}

// function-like macro
#[proc_macro]
/// `combinations!` macro takes a `Vec<T>` and a number _N_ and returns a vector of arrays of length _N_,
/// `Vec<[&T; N]>`, covering all the combinations of _N_ elements from the source vector.
pub fn combinations(input: TokenStream) -> TokenStream {
    let Combinations { arg, n } = parse_macro_input!(input as Combinations);
    let mut indices = syn::punctuated::Punctuated::<_, syn::Token![,]>::new();
    for i in 0..(n.value() as usize) {
        let item = quote! {&pool[indices[#i]]};
        indices.push(item);
    }
    let comb = quote! {
        {
            fn combinations<T>(pool: &[T]) -> Vec<[&T; #n]> {
                let r = #n;
                let mut res = vec![];
                let n = pool.len();
                if r > n {
                    return res;
                }
                let mut indices: Vec<_> = (0..r).collect();
                let reversed: Vec<_> = (0..r).rev().collect();
                res.push([#indices]);
                loop {
                    let idx = (|| {
                        for i in reversed.iter() {
                            if indices[*i] != (*i + n - r) {
                                return Some(*i);
                            }
                        }
                        None
                    })();
                    if let Some(i) = idx {
                        indices[i] += 1;
                        for j in (i + 1)..r {
                            indices[j] = indices[j - 1] + 1;
                        }
                        res.push([#indices]);
                    } else {
                        break;
                    }
                }
                res
            }
            combinations(#arg)
        }
    };
    comb.into()
}
