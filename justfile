demo: build
	$(which time) -v cargo run --release --bin demo

build:
	cargo build --release --bin demo

show: demo
	elm make Circles.elm
